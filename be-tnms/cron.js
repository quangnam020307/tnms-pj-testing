const Cron = require("cron").CronJob;
const { getRandomInt } = require("./utils/randomNumber");

class CronJob {
  count = 0;
  timeInterval = 0;
  job;
  maxTime = 600;

  constructor(_timeInterval, callBack = () => {}, partern = "* * * * * *") {
    this.timeInterval = _timeInterval
      ? _timeInterval
      : getRandomInt(this.maxTime); // get timeInterval on first time

    this.job = new Cron(
      "* * * * * *",
      () => {
        this.count++;
        if (this.count === this.timeInterval) {
          console.log("cronjob is running " + this.timeInterval);
          callBack();

          this.timeInterval = _timeInterval
            ? _timeInterval
            : getRandomInt(this.maxTime);
          this.count = 0;
        }
      },
      null,
      true,
      "Asia/Ho_Chi_Minh"
    );
    this.job.start();
  }
}

module.exports = CronJob;
