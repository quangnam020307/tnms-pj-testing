"use strict";

const { Tracking } = require("../models/tracking.model");

const TrackingService = {
  getAllTracking: (page, limit) => {
    page = page || 1;
    limit = limit || 10;

    return Tracking.paginate({}, { page, limit, sort: { enTryTime: "desc" } });
  },
};
module.exports = TrackingService;
