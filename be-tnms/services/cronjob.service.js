const { Tracking } = require("../models/tracking.model");
const fakerData = require("./faker.service");

const cronJob = {
  fakeTracking: async () => {
    try {
      const data = fakerData.traking();

      const tracking = new Tracking(data);
      await tracking.save();
      console.log(`add new fake traking`, tracking);
    } catch (error) {
      console.log(`can not save data on cronjob ${error}`);
    }
  },
};

module.exports = cronJob;
