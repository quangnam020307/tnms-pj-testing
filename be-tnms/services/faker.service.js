const faker = require("faker");
const config = require("../config");
const { getRandomInt, randomCity } = require("../utils/randomNumber");

const fakerData = {
  traking: () => {
    const type = faker.vehicle.type();
    const image = `${
      config.server.hostName
    }/public/images/download-${getRandomInt(1, 16)}.jpg`;
    const time = getRandomInt(30, 1);
    const day = time < 10 ? `0${time}` : time;
    console.log({ day });
    const enTryTime = new Date(`2021-09-${day}T14:20:10.000Z`);
    return {
      localtion: randomCity(),
      tollNo: getRandomInt(10, 1),
      laneNo: getRandomInt(3, 1),
      vehicleNo: faker.vehicle.vrm(),
      vehicleType: type,
      vehicleSubType: type,
      enTryTime,
      image,
    };
  },
};

module.exports = fakerData;
