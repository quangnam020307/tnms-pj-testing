"use strict";
const mongoose = require("mongoose");

const connectDB = async (mongoUrl) => {
  try {
    await mongoose.connect(mongoUrl, {
      useNewUrlParser: true,
    });
    console.log(`MongoDB connect to ${mongoUrl}`);
  } catch (error) {
    console.log({ error });
  }
};

module.exports = connectDB;
