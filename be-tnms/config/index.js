"use strict";
const dotenv = require("dotenv");
dotenv.config();

// mongoose
const MONGO_HOST = process.env.MONGO_HOST || "mongodb://localhost:27017/tnms";

// server
const SERVER_PORT = process.env.PORT || 3300;
const SERVER_HOSTNAME =
  process.env.SERVER_HOSTNAME || `localhost:${SERVER_PORT}`;

const MONGO = {
  url: MONGO_HOST,
};

const SERVER = {
  hostName: SERVER_HOSTNAME,
  port: SERVER_PORT,
};

const config = {
  mongo: MONGO,
  server: SERVER,
  isProduction: process.env.NODE_ENV === "production",
};

console.log({ config });

module.exports = config;
