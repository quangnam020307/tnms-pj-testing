"use strict";
const express = require("express");
const trackingRouter = require("./tracking.router");

const rootRouter = express.Router();

rootRouter.use("/tracking", trackingRouter);

module.exports = rootRouter;
