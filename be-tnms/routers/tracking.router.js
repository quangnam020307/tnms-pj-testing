"use strict";
const express = require("express");
const { RESPONSE_OBJECT } = require("../models/Response.model");
const { Tracking } = require("../models/tracking.model");
const TrackingService = require("../services/tracking.service");

const trackingRouter = express.Router();

trackingRouter.post("/", async (req, res) => {
  try {
    // const tracking = new Tracking({
    //   localtion: "String",
    //   tollNo: 12,
    //   laneNo: 123,
    //   vehicleNo: "String",
    //   vehicleType: "String",
    //   vehicleSubType: "String",
    //   image: "String",
    // });
    // await tracking.save();
    res.status(201).send("ok");
  } catch (error) {
    res.status(500).send(error);
  }
});

trackingRouter.get("/", async (req, res) => {
  try {
    const pageIndex = req.query.page_index ? +req.query.page_index : 1;
    const pageSize = req.query.page_size ? +req.query.page_size : 10;

    const trackingList = await TrackingService.getAllTracking(
      pageIndex,
      pageSize
    );

    const responseData = RESPONSE_OBJECT();

    responseData.data = trackingList;
    responseData.os = req.os;
    responseData.success = true;

    res.status(200).json(responseData);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = trackingRouter;
