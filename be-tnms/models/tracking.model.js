"use strict";
const { Schema, model } = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const trackingSchema = new Schema({
  localtion: String,
  tollNo: Number,
  laneNo: Number,
  vehicleNo: String,
  vehicleType: String,
  vehicleSubType: String,
  enTryTime: { type: Date, default: Date.now },
  image: String,
});

trackingSchema.plugin(mongoosePaginate);

const Tracking = model("Tracking", trackingSchema);

module.exports = {
  trackingSchema,
  Tracking,
};
