const RESPONSE_OBJECT = () => {
  return {
    data: {},
    success: false,
  };
};

module.exports = { RESPONSE_OBJECT };
