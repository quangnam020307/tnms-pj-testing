const { cityListIndia } = require("../constant");

const getRandomInt = (max = 1, min = 0) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
};

const randomCity = () => cityListIndia[getRandomInt(cityListIndia.length)];

module.exports = {
  getRandomInt,
  randomCity,
};
