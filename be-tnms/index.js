"use strict";
const express = require("express");
const cors = require("cors");
const path = require("path");

const config = require("./config");
const connectDB = require("./config/mongoose");
const rootRouter = require("./routers/root.router");
const Cron = require("./cron");
const cronJob = require("./services/cronjob.service");
const os = require("os");
const hostName = os.hostname();
const app = express();

app.use(cors());
app.use(express.json());

const { mongo } = config;
connectDB(mongo.url);

console.log(hostName);

app.use((req, res, next) => {
  req.os = hostName;
  console.log({ originalUrl: req.originalUrl });
  console.info("Request data:", req.body);
  next();
});

app.get("/api/v1/swarm", (req, res, next) => {
  res.send({
    data: ["run"],
    hostName,
  });
});

app.use("/api/v1", rootRouter);

const publicPathDirectory = path.join(__dirname, "./public");
app.use("/public", express.static(publicPathDirectory));

// auto add a tracking
new Cron(0, cronJob.fakeTracking);

app.listen(process.env.PORT || 3300, () => {
  console.log(`App listening on port ${process.env.PORT || 3300}!`);
});
