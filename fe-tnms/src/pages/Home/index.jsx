import { Col, Layout, Row } from "antd";
import React from "react";
import styles from "./styles.module.css";
import UploadVideo from "./components/Upload";
import Table from "./components/Table";
import SearchBar from "../../component/SearchBar";

function Home() {
  const handleChangeFile = (file) => {
    console.log({ file });
  };
  return (
    <Layout className={styles.layout}>
      <Row className={styles.root__card}>
        <Col span={8} className={styles.media__tab}>
          <UploadVideo />
        </Col>
        <Col span={16} className={styles.table__tab}>
          <SearchBar />
          <Table />
        </Col>
      </Row>
    </Layout>
  );
}

export default Home;
