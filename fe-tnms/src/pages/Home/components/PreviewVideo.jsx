import React from "react";
import { Player } from "video-react";

const PreviewVideo = ({ previewSrc }) => {
  return (
    <Player>
      <source src={previewSrc} />
    </Player>
  );
};

export default PreviewVideo;
