import React, { useState } from "react";
import { Upload, Button, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import PreviewVideo from "./PreviewVideo";
import useUploadVideo from "../../../hooks/useUploadVideo";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

const UploadVideo = () => {
  const [state, setState] = useState({
    fileList: [],
    uploading: false,
  });

  const { uploadVideo: uploadVideoServ } = useUploadVideo();

  const [previewSrc, setPreviewSrc] = useState(null);

  const handleUpload = async () => {
    const { fileList } = state;
    if (fileList) {
      console.log({ fileList });
      uploadVideoServ(fileList);
    }
  };

  const onChangeFile = () => {
    if (fileList) {
      getBase64(fileList[0]).then((base64) => {
        setPreviewSrc(base64);
      });
    }
  };

  const { uploading, fileList } = state;
  const uploadFileProps = {
    onRemove: (file) => {
      setPreviewSrc(null);
      setState((state) => {
        const index = state.fileList.indexOf(file);
        const newFileList = state.fileList.slice();
        newFileList.splice(index, 1);
        return {
          fileList: newFileList,
        };
      });
    },
    beforeUpload: (file) => {
      setState((_state) => ({
        fileList: [..._state.fileList, file],
      }));
      return false;
    },
    fileList,
  };

  console.log({ previewSrc });

  return (
    <>
      {previewSrc && <PreviewVideo previewSrc={previewSrc} />}

      <Upload
        {...uploadFileProps}
        onChange={onChangeFile}
        accept="video/*"
        multiple={false}
      >
        <Button icon={<UploadOutlined />}>Select File</Button>
      </Upload>
      <Button
        type="primary"
        onClick={handleUpload}
        disabled={fileList.length === 0}
        loading={uploading}
        style={{ marginTop: 16 }}
      >
        {uploading ? "Uploading" : "Start Upload"}
      </Button>
    </>
  );
};

export default UploadVideo;
