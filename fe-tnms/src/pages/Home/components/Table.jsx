import React from "react";
import { Table, Image } from "antd";
import formatDate from "date-fns/format";
import useFetchTracking from "../../../hooks/useFetchTracking";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import useSearchTracking from "../../../hooks/useSearchTracking";

const columns = [
  {
    title: "ID",
    dataIndex: "ID",
    key: "ID",
    render: (_, __, index) => <>{index + 1}</>,
  },
  {
    title: "LOCALTION",
    dataIndex: "localtion",
    key: "localtion",
  },
  {
    title: "TOLL_NO",
    dataIndex: "tollNo",
    key: "tollNo",
  },
  {
    title: "LANE_NO",
    dataIndex: "laneNo",
    key: "laneNo",
  },
  {
    title: "VEHICLE_NO",
    dataIndex: "vehicleNo",
    key: "vehicleNo",
  },
  {
    title: "VEHICLE_TYPE",
    dataIndex: "vehicleType",
    key: "vehicleType",
  },
  {
    title: "VEHICLE_SUB_TYPE",
    dataIndex: "vehicleSubType",
    key: "vehicleSubType",
  },
  {
    title: "ENTRY_TIME",
    dataIndex: "enTryTime",
    key: "enTryTime",
    render: (value) => <>{formatDate(new Date(value), "dd/MM/yyyy kk:mm")}</>,
  },
  {
    title: "IMAGE",
    dataIndex: "image",
    key: "image",
    render: (value) => <Image width={150} height={150} src={value} />,
  },
];

let intervalRefetch = null;

const TableTracking = () => {
  const history = useHistory();
  const location = useLocation();

  const [dataTracking = {}, { refetchData }] = useFetchTracking(1, 10);

  const [filterTracking] = useSearchTracking();

  React.useEffect(() => {
    intervalRefetch = setInterval(() => {
      refetchData();
    }, 30000);
    return () => {
      clearInterval(intervalRefetch);
    };
  }, [refetchData]);

  const data = filterTracking(dataTracking && dataTracking.docs);

  const { limit = 1, page = 10, total = 100 } = dataTracking || {};

  const paginatorData = React.useMemo(() => {
    return {
      current: page,
      pageSize: limit,
      total,
      onchange: ({ current, pageSize }) => {
        const currentQueryParams = location?.state || {};
        const queryParams = {
          ...currentQueryParams,
          page_index: current,
          page_size: pageSize,
        };
        const params = queryString.stringify(queryParams);
        console.log({ params });
        history.push({
          pathname: "/",
          search: `?${params}`,
          state: {
            ...queryParams,
          },
        });
      },
    };
  }, [page, limit, total, location?.state, history]);

  return (
    <Table
      columns={columns}
      dataSource={data || []}
      pagination={paginatorData}
      onChange={paginatorData.onchange}
    />
  );
};

export default TableTracking;
