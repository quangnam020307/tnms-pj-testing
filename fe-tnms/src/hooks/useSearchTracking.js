import React from "react";
import { useLocation } from "react-router-dom";

const useSearchTracking = () => {
  const location = useLocation();
  const {
    vehicle_no: vehicleNo,
    vehicle_type: vehicleType,
    vehicle_subType: vehicleSubType,
    from,
    to,
  } = location?.state || {};

  const searchValue = React.useMemo(
    () =>
      Object.entries({
        vehicleNo,
        vehicleType,
        vehicleSubType,
        from,
        to,
      }).reduce((values, [key, value]) => {
        if (!!value || +value === 0) values[key] = value;
        return values;
      }, {}),
    [from, to, vehicleNo, vehicleSubType, vehicleType]
  );

  const filterTracking = React.useCallback(
    (data) => {
      if (!data || !data.length) return data;
      const result = Object.entries(searchValue).reduce(
        (currentValue, [key, value]) => {
          if (key === "from") {
            return currentValue.filter((item) => {
              const timeEnTryTime = Math.round(
                new Date(item.enTryTime).getTime() / 1000
              );
              return value < timeEnTryTime;
            });
          }

          if (key === "to") {
            return currentValue.filter((item) => {
              const timeEnTryTime = Math.round(
                new Date(item.enTryTime).getTime() / 1000
              );
              return value > timeEnTryTime;
            });
          }

          return currentValue.filter((item) => {
            const dataItem = item[key]?.toLocaleLowerCase();
            return dataItem?.includes(value.toLocaleLowerCase());
          });
        },
        data
      );

      console.log({ result, searchValue });
      return result;
    },
    [searchValue]
  );

  return [filterTracking];
};

export default useSearchTracking;
