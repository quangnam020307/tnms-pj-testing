import React from "react";
import uploadApi from "../services/upload";

const useUploadVideo = () => {
  const uploadVideo = React.useCallback((file) => {
    var data = new FormData();
    data.append("file", file[0]);
    console.log({ data, file });
    return uploadApi.video(data);
  }, []);

  return { uploadVideo };
};

export default useUploadVideo;
