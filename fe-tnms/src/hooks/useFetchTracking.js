import { useCallback, useEffect, useState } from "react";
import trackingApi from "../services/trackingApi";
import { useLocation } from "react-router-dom";

const useFetchTracking = () => {
  const [dataTracking, setDataTracking] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const location = useLocation();

  const { page_index: _pageIndex = 1, page_size: _pageSize = 50 } =
    location?.state || {};

  const fetchTracking = useCallback(async () => {
    try {
      setIsLoading(true);
      const data = await trackingApi.getAll(_pageIndex, _pageSize);
      setDataTracking(data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);

      throw error;
    }
  }, [_pageIndex, _pageSize]);

  const refetchData = useCallback(() => {
    fetchTracking();
  }, [fetchTracking]);

  useEffect(() => {
    fetchTracking();
  }, [fetchTracking]);

  return [
    dataTracking,
    {
      refetchData,
      isLoading,
    },
  ];
};

export default useFetchTracking;
