import tnmsAxiosClient from "./tnmsAxiosClient";

const uploadApi = {
  video: (formData) => {
    const url = "/video_upload";
    return tnmsAxiosClient.post(url, formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    });
  },
};

export default uploadApi;
