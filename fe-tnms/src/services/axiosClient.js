import axios from "axios";
import queryString from "query-string";

const _baseUrl = process.env.REACT_APP_BE_DOMAIN;

const baseURL = _baseUrl || "http://localhost:3300/api/v1";

console.log({ _baseUrl, baseURL });

const axiosClient = axios.create({
  baseURL,
  headers: {
    "content-type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.request.use((config) => {
  return config;
});

axiosClient.interceptors.response.use(
  (response) => {
    if (response && response.data && response.data.success) {
      return response.data.data;
    }
    return response;
  },
  (error) => {
    throw error;
  }
);

export default axiosClient;
