import axiosClient from "./axiosClient";

const trackingApi = {
  getAll: (page_index, page_size) => {
    const url = "/tracking";

    return axiosClient.get(url, { params: { page_index, page_size } });
  },
};

export default trackingApi;
