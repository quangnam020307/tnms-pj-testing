import axios from "axios";
import queryString from "query-string";

const _baseUrl = process.env.REACT_APP_BE_TNMS_DOMAIN;

const baseURL = _baseUrl || "http://199.192.26.114:8000/";

console.log({ _baseUrl, baseURL });

const tnmsAxiosClient = axios.create({
  baseURL,
  headers: {
    "content-type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

tnmsAxiosClient.interceptors.request.use((config) => {
  return config;
});

tnmsAxiosClient.interceptors.response.use(
  (response) => {
    if (response && response.data && response.data.success) {
      return response.data.data;
    }
    return response;
  },
  (error) => {
    throw error;
  }
);

export default tnmsAxiosClient;
