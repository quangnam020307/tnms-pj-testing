import { DatePicker, Form } from "antd";
import React from "react";

const DateAntdForm = (props) => {
  console.log({ props });
  return (
    <Form.Item {...props}>
      <DatePicker />
    </Form.Item>
  );
};

export default DateAntdForm;
