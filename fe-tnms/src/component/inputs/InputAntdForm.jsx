import { Form, Input } from "antd";
import React from "react";

const InputAntdForm = (props) => {
    console.log({props});
  return (
    <Form.Item {...props}>
      <Input />
    </Form.Item>
  );
};

export default InputAntdForm;
