import { Col, Form, Row } from "antd";
import React from "react";
import InputAntdForm from "./inputs/InputAntdForm";
import DateAntdForm from "./inputs/DateAntdForm";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";

const SearchBar = () => {
  const history = useHistory();
  const location = useLocation();

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleChange = (
    _,
    { vehicleNo, vehicleType, vehicleSubType, from, to }
  ) => {
    const currentQueryParams = location?.state || {};
    console.log({ from });
    const queryParams = {
      ...currentQueryParams,
      vehicle_no: vehicleNo ? vehicleNo : undefined,
      vehicle_type: vehicleType ? vehicleType : undefined,
      vehicle_subType: vehicleSubType ? vehicleSubType : undefined,
      from: from?.unix(),
      to: to?.unix(),
    };
    const params = queryString.stringify(queryParams);
    history.push({
      pathname: "/",
      search: `?${params}`,
      state: {
        ...queryParams,
      },
    });
  };

  return (
    <div>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        onValuesChange={handleChange}
      >
        <Row>
          <Col span={8}>
            <InputAntdForm label="Vehicle No" name="vehicleNo" />
          </Col>
          <Col span={8}>
            <InputAntdForm label="Vehicle Type" name="vehicleType" />
          </Col>
          <Col span={8}>
            <InputAntdForm label="Vehicle SubType" name="vehicleSubType" />
          </Col>
          <Col span={8}>
            <DateAntdForm label="Date From" name="from" />
          </Col>
          <Col span={8}>
            <DateAntdForm label="To" name="to" />
          </Col>
        </Row>
      </Form>
    </div>
  );
};
export default SearchBar;
